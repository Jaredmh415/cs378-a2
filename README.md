Ahir, Jared, Jack

**Character Requirements - Jared**

Idle: The base state of the character when not moving.

Movement: Moves with the WASD keys and presses shift to start sprinting.

Camera/Turn: The camera turns with the mouse and can press right click to set the position of the camera behind the character. The x-movement of the mouse is set to yaw and the y movement of the mouse was set to pitch. We changed the rotation of the player controller to the yaw of the camera boom.

Crouch: We used the built in crouch function to change the capsule size and set the movement speed to crouch walk speed.

Pick Up: We raycast from the character in front of them and got the intersecting objects and performed a pick up. We used a boolean to check if an object was already picked up and if it was, we dropped the object.

Pull: When the lever is pulled, we destroyed the responder. We used a delegate such that when the delegate was interacted with, we triggered the destruction of the responder.

**Interactables - Ahir**

We had three different interactables. 

Level Transition: This one was for moving through levels, which was done through Jack's level transitions that move players to the next level.

Pickupable: This one was a tough one to get working, but after playing around with ray casting from the cahracter in front of them and a few tutorials, we got it to easily be picked up without other objects being able to be picked up as well by checking the actor's class.

Lever/Response: This was a pretty simple one, we did it using delegate trigger responses like lab 3 and overlapping hitboxes. These would then destroy the response interactables.

**Arena Requirements - Jack**

We created 2 maps with multiple platforming segments to highlight both the crouch and jump functions of the character. The first level has 3 level transitions that will take the player into the second level.

Demo Link: https://youtu.be/E0vKXYM8Ut0

Screenshots are in the respository under "Screenshots"
