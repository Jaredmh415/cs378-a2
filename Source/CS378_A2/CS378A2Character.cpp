// Fill out your copyright notice in the Description page of Project Settings.

#include "GameFramework/CharacterMovementComponent.h"
#include "Camera/CameraComponent.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/SpringArmComponent.h"
#include "CS378A2Character.h"
#include "LeverActor.h"
#include "PickupActor.h"

// Sets default values
ACS378A2Character::ACS378A2Character()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
    
    // Create a camera boom...
    CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
    CameraBoom->SetupAttachment(RootComponent);
    CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when ship does
    CameraBoom->TargetArmLength = 400.f;
    CameraBoom->SetRelativeRotation(FRotator(0.f, 0.f, 0.f));
    CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

    // Create a camera...
    CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("OvertheShoulderCamera"));
    CameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
    CameraComponent->bUsePawnControlRotation = false;    // Camera does not rotate relative to arm
    
    GetCharacterMovement()->NavAgentProps.bCanCrouch = true;

}

// Called when the game starts or when spawned
void ACS378A2Character::BeginPlay()
{
	Super::BeginPlay();
    WalkSpeed = 500.0f;
    SprintSpeed = 1200.0f;
    CrouchSpeed = 100.0f;
    WalkingState = true;
    CrouchingState = false;
    PickupState = false;
    GetCharacterMovement()-> MaxWalkSpeedCrouched = CrouchSpeed;
	
}

// Called every frame
void ACS378A2Character::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ACS378A2Character::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void ACS378A2Character::MoveForward(float value)
{
    FVector Direction = FRotationMatrix(Controller->GetControlRotation()).GetScaledAxis(EAxis::X);
    AddMovementInput(Direction, value);
}
void ACS378A2Character::MoveRight(float value)
{
    FVector Direction = FRotationMatrix(Controller->GetControlRotation()).GetScaledAxis(EAxis::Y);
    AddMovementInput(Direction, value);
}
void ACS378A2Character::JumpUp()
{
    this->Jump();
}
bool ACS378A2Character::CanPerformAction(ECharacterActionStateEnum updatedAction)
{
    switch(CharacterActionState)
    {
        case ECharacterActionStateEnum::IDLE:
            return true;
            break;
        case ECharacterActionStateEnum::MOVE:
            if(updatedAction != ECharacterActionStateEnum::INTERACT)
            {
                return true;
            }
            break;
        case ECharacterActionStateEnum::SPRINTING:
            if(updatedAction != ECharacterActionStateEnum::INTERACT)
            {
                return true;
            }
            break;
        case ECharacterActionStateEnum::JUMP:
            if (updatedAction == ECharacterActionStateEnum::IDLE || updatedAction == ECharacterActionStateEnum::MOVE)
            {
                return true;
            }
            break;
        case ECharacterActionStateEnum::CROUCH:
            if(updatedAction == ECharacterActionStateEnum::JUMP)
            {
                return false;
            }
            return true;
            break;
        case ECharacterActionStateEnum::INTERACT:
            return false;
    }
    return false;
}
void ACS378A2Character::BeginInteraction()
{
    if(GEngine)
    {
        GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, TEXT("Begin Interaction"));
    }
    TArray< AActor*>OverlappingActors;
    this->GetOverlappingActors(OverlappingActors);
    if (GEngine) {
        GEngine->AddOnScreenDebugMessage(-1, 1.0, FColor::Purple, FString::Printf(TEXT("%d"), OverlappingActors.Num()));
    }
    for (AActor* actor : OverlappingActors)
    {
        if (actor->IsA(ALeverActor::StaticClass())) {
            ALeverActor* ala = (ALeverActor*)actor;
            return LeverAction(ala);
        }
    }
    return PickUpObject();
}

void ACS378A2Character::LeverAction(ALeverActor* ala) {
    GetWorld()->GetTimerManager().SetTimer(InteractionTimerHandle, this, &ACS378A2Character::EndInteraction, 3.0f, false);
    ala->OnTriggerDelegate.Broadcast();
}

void ACS378A2Character::EndInteraction()
{
    if(GEngine)
    {
        GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, TEXT("End Interaction"));
    }
    UpdateActionState(ECharacterActionStateEnum::IDLE);
    
}
void ACS378A2Character::UpdateActionState(ECharacterActionStateEnum newAction)
{
    if (newAction == ECharacterActionStateEnum::MOVE || newAction == ECharacterActionStateEnum::IDLE)
    {
        if (FMath::Abs(GetVelocity().Size()) <= 0.01f)
        {
            CharacterActionState = ECharacterActionStateEnum::IDLE;
            EnableWalk();
        }
        else
        {
            CharacterActionState = ECharacterActionStateEnum::MOVE;
            
        }
    }
    else if(newAction == ECharacterActionStateEnum::SPRINTING)
    {
       if (FMath::Abs(GetVelocity().Size()) <= 0.01f)
        {
            CharacterActionState = ECharacterActionStateEnum::IDLE;
            EnableWalk();
        }
        else
        {
            CharacterActionState = ECharacterActionStateEnum::SPRINTING;
            
        }
    }
    else
    {
        CharacterActionState = newAction;
    }
}
void ACS378A2Character::CrouchToggle()
{
    if (CrouchingState)
    {
        GetCharacterMovement()-> MaxWalkSpeed = WalkSpeed;
        UnCrouch();
        CrouchingState = false;
    }
    else
    {
        
        GetCharacterMovement()-> MaxWalkSpeed = CrouchSpeed;
        GetCharacterMovement()-> MaxWalkSpeedCrouched = CrouchSpeed;
        Crouch();
        CrouchingState = true;
    }
    
}
void ACS378A2Character::DetermineInteractAction()
{
    
}
void ACS378A2Character::PickUpObject()
{
    APlayerCameraManager* CurrentCamera = GetWorld()->GetFirstPlayerController()->PlayerCameraManager;		
    // Get the player camera position
    FVector CameraLocation = GetWorld()->GetFirstPlayerController()->PlayerCameraManager->GetCameraLocation();
    FVector CharacterLocation = GetWorld()->GetFirstPlayerController()->GetPawn()->GetActorLocation();
    FVector EndVector = this->GetActorForwardVector() * 50;
    //FVector ForwardVector = this->GetActorForwardVector(this->GetActorRotation()) * 50;
    FHitResult result(ForceInit);
    const FName TraceTag("MyTraceTag");
    GetWorld()->DebugDrawTraceTag = TraceTag;
    FCollisionQueryParams RV_TraceParams = FCollisionQueryParams(FName(TEXT("RV_Trace")), true, this);
    RV_TraceParams.TraceTag = TraceTag;
    this->GetWorld()->LineTraceSingleByChannel(result, CharacterLocation, EndVector + CharacterLocation, ECC_GameTraceChannel1, RV_TraceParams);
    if (result.bBlockingHit) {
        if (GEngine) {
            GEngine->AddOnScreenDebugMessage(-1, 1.0, FColor::Orange, FString::Printf(TEXT("Hit an object")));
        }
        AActor* actor = result.GetActor(); //the hit actor if there is one
        if (actor) {
            if (actor->IsA(APickupActor::StaticClass())) {
                if (GEngine) {
                    GEngine->AddOnScreenDebugMessage(-1, 1.0, FColor::Blue, FString::Printf(TEXT("Hit a pickup actor")));
                }
            }
        }
    }
    else {
        if (GEngine) {
            GEngine->AddOnScreenDebugMessage(-1, 1.0, FColor::Orange, FString::Printf(TEXT("Didn't hit an object")));
        }
    }
    
    GetWorld()->GetTimerManager().SetTimer(InteractionTimerHandle, this, &ACS378A2Character::EndInteraction, 1.0f, false);
}

void ACS378A2Character::Pickup() {

}

void ACS378A2Character::EnableSprint()
{
    if(CrouchingState)
    {
//        UnCrouch();
        CrouchingState = false;
    }
    GetCharacterMovement()-> MaxWalkSpeed = SprintSpeed;
    WalkingState = false;
}
void ACS378A2Character::EnableWalk()
{
    GetCharacterMovement()-> MaxWalkSpeed = WalkSpeed;
    WalkingState = true;
}
void ACS378A2Character::MouseXMove(float value)
{
    FRotator oldRotation = CameraBoom->GetRelativeRotation();
    oldRotation.Yaw = oldRotation.Yaw+value;
    CameraBoom->SetRelativeRotation(oldRotation);
}
void ACS378A2Character::MouseYMove(float value)
{
    FRotator oldRotation = CameraBoom->GetRelativeRotation();
    oldRotation.Pitch = oldRotation.Pitch+value;
//    float pitch = oldRotation.Pitch;
//    float roll = oldRotation.Roll+value;
//    float yaw = oldRotation.Yaw;
    CameraBoom->SetRelativeRotation(oldRotation);
}
void ACS378A2Character::MouseRightMove()
{
//    FRotator oldRotation = CameraBoom->GetComponentRotation();
    FRotator charRotation = this->GetActorRotation();
    FRotator cameraRotation = CameraBoom->GetComponentRotation();
//    FRotator fixRotation = FRotator(cha, cameraRotation.Yaw, 0.0f);
    charRotation.Yaw = cameraRotation.Yaw;
//    this->SetActorRelativeRotation(fixRotation);
//    GetPlayerController();
    
    
    UGameplayStatics::GetPlayerController(GetWorld(), 0)->SetControlRotation(charRotation);

    
//    this->SetActorRotation(fixRotation);
    
}

