// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TimerManager.h"
#include "GameFramework/Character.h"
#include "LeverActor.h"
#include "CS378A2Character.generated.h"

UENUM(BlueprintType)
enum class ECharacterActionStateEnum: uint8 {
    IDLE UMETA(DisplayName = "Idling"),
    MOVE UMETA(DisplayName = "Moving"),
    JUMP UMETA(DisplayName = "Jumping"),
    INTERACT UMETA(DisplayName = "Interacting"),
    CROUCH UMETA(DisplayName = "Crouching"),
    DOUBLEJUMP UMETA(DisplayName = "DoubleJumping"),
    PICKUP UMETA(DisplayName = "PickedUpObject"),
    SPRINTING UMETA(DisplayName = "Running")
};

UCLASS()
class CS378_A2_API ACS378A2Character : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ACS378A2Character();
    
    FORCEINLINE class UCameraComponent * GetCameraComponent() const { return CameraComponent;}
    FORCEINLINE class USpringArmComponent * GetCameraBoom() const {return CameraBoom; }
    
    UPROPERTY(BlueprintReadWrite, EditAnywhere)
    float SprintSpeed;
    
    UPROPERTY(BlueprintReadWrite, EditAnywhere)
    float WalkSpeed;
    
    UPROPERTY(BlueprintReadWrite, EditAnywhere)
    float CrouchSpeed;
    

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
    
    UPROPERTY(Category = Camera, EditAnywhere, BlueprintReadWrite)
    class UCameraComponent * CameraComponent;
    
    UPROPERTY(Category = Camera, EditAnywhere, BlueprintReadWrite)
    class USpringArmComponent * CameraBoom;
    
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
    ECharacterActionStateEnum CharacterActionState;
    
    UPROPERTY(VisibleANywhere, BlueprintReadOnly)
    bool WalkingState;
    
    UPROPERTY(VisibleANywhere, BlueprintReadOnly)
    bool CrouchingState;

    UPROPERTY(BlueprintReadWrite, EditAnywhere)
        bool PickupState;
    
    FTimerHandle InteractionTimerHandle;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
    
    //this will map to pull and pickup depending on the event will affect state and other properties
    UFUNCTION(BlueprintImplementableEvent)
    void InteractPressed();
    
    UFUNCTION(BlueprintImplementableEvent)
    void JumpMovementPressed();
    
    UFUNCTION(BlueprintImplementableEvent)
    void ForwardMovementAxis(float value);
    
    UFUNCTION(BlueprintImplementableEvent)
    void RightMovementAxis(float value);
    
    UFUNCTION(BlueprintImplementableEvent)
    void MouseXPressed(float value);

    UFUNCTION(BlueprintImplementableEvent)
    void MouseYPressed(float value);
    
    UFUNCTION(BlueprintImplementableEvent)
    void MouseRightPressed();
    
    UFUNCTION(BlueprintImplementableEvent)
    void CrouchPressed();
    
    UFUNCTION(BlueprintImplementableEvent)
    void SprintActionPressed();

    UFUNCTION(BlueprintImplementableEvent)
        void PickupPressed();
    
    UFUNCTION(BlueprintCallable)
    void CrouchToggle();
    
    UFUNCTION(BlueprintCallable)
    void MouseRightMove();
    
    UFUNCTION(BlueprintCallable)
    void MouseXMove(float value);
    
    UFUNCTION(BlueprintCallable)
    void MouseYMove(float value);
    
    UFUNCTION(BlueprintCallable)
    void MoveForward(float value);
    
    UFUNCTION(BlueprintCallable)
    void MoveRight(float value);

    UFUNCTION(BlueprintCallable)
        void Pickup();
    
    //will return an enum to determine interaction type
    UFUNCTION(BlueprintCallable)
    void DetermineInteractAction();
    
    UFUNCTION(BlueprintCallable)
    void JumpUp();
    
    UFUNCTION(BlueprintCallable)
    bool CanPerformAction(ECharacterActionStateEnum updatedAction);
    
    //begin interaction of pulling lever
    UFUNCTION(BlueprintCallable)
    void BeginInteraction();
    
    //lever pull complete
    UFUNCTION(BlueprintCallable)
    void EndInteraction();
    
    //might need to pass it object if pulled earlier dont calcaluate twice ie determineinteractaction
    UFUNCTION(BlueprintCallable)
    void PickUpObject();
    
    UFUNCTION(BlueprintCallable)
    void UpdateActionState(ECharacterActionStateEnum newAction);
    
    UFUNCTION(BlueprintCallable)
    void EnableSprint();
    
    UFUNCTION(BlueprintCallable)
    void EnableWalk();

    UFUNCTION(BlueprintCallable)
        void LeverAction(ALeverActor* ala);

};
