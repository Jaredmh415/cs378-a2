// Fill out your copyright notice in the Description page of Project Settings.


#include "CS378A2PlayerController.h"
#include "CS378A2Character.h"

ACS378A2PlayerController::ACS378A2PlayerController()
{
    
}

void ACS378A2PlayerController::SetupInputComponent()
{
    Super::SetupInputComponent();
    InputComponent->BindAction("JumpMovement", IE_Pressed, this, &ACS378A2PlayerController::JumpAction);
    InputComponent->BindAction("InteractionKey", IE_Pressed, this, &ACS378A2PlayerController::InteractAction);
    InputComponent->BindAction("CrouchKey", IE_Pressed, this, &ACS378A2PlayerController::CrouchAction);
    InputComponent->BindAxis("ForwardMovement", this, &ACS378A2PlayerController::ForwardMove);
    InputComponent->BindAxis("RightMovement", this, &ACS378A2PlayerController::RightMove);
    InputComponent->BindAxis("MouseXInput", this, &ACS378A2PlayerController::MouseXAction);
    InputComponent->BindAxis("MouseYInput", this, &ACS378A2PlayerController::MouseYAction);
    InputComponent->BindAction("SprintToggle", IE_Pressed, this, &ACS378A2PlayerController::SprintAction);
    InputComponent->BindAction("MouseRightPressed", IE_Pressed, this, &ACS378A2PlayerController::MouseRightAction);
    InputComponent->BindAction("Pickup", IE_Pressed, this, &ACS378A2PlayerController::PickupAction);
    
}
void ACS378A2PlayerController::InteractAction()
{
    ACS378A2Character * character = Cast<ACS378A2Character>(this->GetCharacter());
    if (character)
    {
        character->InteractPressed();
    }
}
void ACS378A2PlayerController::ForwardMove(float value)
{
//    if (GEngine)
//    {
//     GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Black, FString::Printf(TEXT("Forward Controller: %f"), value));
//    }
    ACS378A2Character * character = Cast<ACS378A2Character>(this->GetCharacter());
    if (character)
    {
        character->ForwardMovementAxis(value);
    }
}
void ACS378A2PlayerController::RightMove(float value)
{
    ACS378A2Character * character = Cast<ACS378A2Character>(this->GetCharacter());
    if (character)
    {
        character->RightMovementAxis(value);
    }
}
void ACS378A2PlayerController::JumpAction()
{
    ACS378A2Character * character = Cast<ACS378A2Character>(this->GetCharacter());
    if (character)
    {
        character->JumpMovementPressed();
    }
}
void ACS378A2PlayerController::SprintAction()
{
    ACS378A2Character * character = Cast<ACS378A2Character>(this->GetCharacter());
    if (character)
    {
        character->SprintActionPressed();
    }
}
void ACS378A2PlayerController::CrouchAction()
{
    ACS378A2Character * character = Cast<ACS378A2Character>(this->GetCharacter());
    if (character)
    {
        character->CrouchPressed();
    }
}
void ACS378A2PlayerController::MouseXAction(float value)
{
    ACS378A2Character * character = Cast<ACS378A2Character>(this->GetCharacter());
    if (character)
    {
        character->MouseXPressed(value);
    }
}
void ACS378A2PlayerController::MouseYAction(float value)
{
    ACS378A2Character * character = Cast<ACS378A2Character>(this->GetCharacter());
    if (character)
    {
        character->MouseYPressed(value);
    }
}
void ACS378A2PlayerController::MouseRightAction()
{
    ACS378A2Character * character = Cast<ACS378A2Character>(this->GetCharacter());
    if (character)
    {
        character->MouseRightPressed();
    }
}
void ACS378A2PlayerController::PickupAction()
{
    ACS378A2Character* character = Cast<ACS378A2Character>(this->GetCharacter());
    if (character)
    {
        character->PickupPressed();
    }
}

