// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "CS378A2PlayerController.generated.h"

/**
 * 
 */
UCLASS()
class CS378_A2_API ACS378A2PlayerController : public APlayerController
{
    GENERATED_BODY()
    
public:
    ACS378A2PlayerController();
    
    UFUNCTION()
    void JumpAction();
    UFUNCTION()
    void InteractAction();
    UFUNCTION()
    void ForwardMove(float value);
    UFUNCTION()
    void RightMove(float value);
    UFUNCTION()
    void SprintAction();
    UFUNCTION()
    void CrouchAction();
    UFUNCTION()
    void MouseXAction(float value);
    UFUNCTION()
    void MouseYAction(float value);
    UFUNCTION()
    void MouseRightAction(); 
    UFUNCTION()
    void PickupAction();
   
    
    
protected:
    
    virtual void SetupInputComponent() override;
    
};
