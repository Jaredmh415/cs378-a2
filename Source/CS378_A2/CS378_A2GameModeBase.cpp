// Copyright Epic Games, Inc. All Rights Reserved.


#include "CS378A2Character.h"
#include "CS378A2PlayerController.h"
#include "CS378_A2GameModeBase.h"


ACS378_A2GameModeBase::ACS378_A2GameModeBase()
{
    static ConstructorHelpers::FObjectFinder<UClass> pawnBPClass(TEXT("Blueprint'/Game/Blueprints/CS378A2CharacterBP.CS378A2CharacterBP_C'"));
     if (pawnBPClass.Object)
    {
        if (GEngine)
        {
         GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, TEXT("PawnBP Found"));
        }
        UClass* pawnBP = (UClass* )pawnBPClass.Object;
        DefaultPawnClass = pawnBP;
    }
    else
    {
        GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, TEXT("PawnBP Not Found"));
        DefaultPawnClass = ACS378A2Character::StaticClass();
    }
    PlayerControllerClass = ACS378A2PlayerController::StaticClass();
    
    
}

