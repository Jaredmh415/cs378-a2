// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "LeverActor.h"
#include "ResponseActor.generated.h"

UCLASS()
class CS378_A2_API AResponseActor : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AResponseActor();

	FORCEINLINE UStaticMeshComponent* GetMeshComponent() const { return MeshComponent; }

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UStaticMeshComponent* MeshComponent;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		ALeverActor* LeverActor;

	UFUNCTION(BlueprintCallable)
		void Respond();
};
